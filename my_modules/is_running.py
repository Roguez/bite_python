# La idea aquí es hacer funciones para llamar en procesos en los que el ordenador se
# tira un buen rato como descargado algo o así, y que aquí muestre pues que está
# trabajando que no se quede ahí la terminal en blanco y uno sin saber si se ha
# quedado colgado o no. como aquello:

a = ["[-]","[\]","[|]","[/]","[-]","[\]","[|]","[/]"]

Esto es útil para cuando estás descargándote algo entonces tendrías dos procesos,
este y el de la descarga que sería aśincrono no porque está procesando la descarga de
bits pero si que puede ser multiprocessing para no incidir en ella, pero con callback
para que cuando termine el download termine esto

Este patrón puede ser muy útil y puedes tenerlo implementado en un montón de sitios.