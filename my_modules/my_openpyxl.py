import openpyxl
import logging

# -----------------------------------------------------------------------------

def get_column_content(xl_file, xl_sheet = None, column_index = None,
                       min_row = None, max_row = None, row_step = 1):

# Devuelve el contenido en forma de diccionario de una columna concreta.

    # Abrimos el fichero:

    wb = openpyxl.load_workbook(xl_file, read_only = True)

    # Abrimos la hoja:

    if xl_sheet == None:
        sheet_list = wb.get_sheet_names()
        print("Este es el listado de sheet disponibles:")
        print(sheet_list)
        xl_sheet = input("Por favor seleccione una: ")
    ws = wb.get_sheet_by_name(xl_sheet)

    # Obtenemos la columna:

    if column_index == None:
        max_col = ws.max_column
        print("Las columnas disponibles son: ")
        print(range(1,max_col))
        column_index = int(input("Por favor, introduzca el indice de la columna: "))
        print(type(column_index))

    # Consultamos las celdas a extraer de dicha columna:

    if min_row == None: min_row = 2
    if max_row == None: max_row = ws.max_row
    elif max_row > ws.max_row:
         max_row = ws.max_row

    # Extraemos las celdas:

    # Es importante forzar que nos devuelva un str y no un objeto de openpyxl
    # porque si no luego en caso de leer una celda vacia nos dara error
    # posterioremente cuando queramos utilizar el valor de esa celda como un string.

    columna = {}

    for i in range(min_row, max_row, row_step):
        columna[i-min_row] = str(ws.cell(row=i, column=column_index).value)
        print(i)
    return columna

# -------------------------------------------------------------------------------
