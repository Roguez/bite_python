# https://www.geeksforgeeks.org/python-reading-contents-of-pdf-using-ocr-optical-character-recognition/
# https://nanonets.com/blog/ocr-with-tesseract/

from PIL import Image       # Pillow: Python Picture Library
import pytesseract          # OCR tool for Python
from pdf2image import convert_from_path

#--------- Parte 1: Convertimos cada pagina del pdf en una imagen jpg

# Path of the pdf

PDF_file = "/home/roguez/Coding/Projects_Data/OCR_PDF/Japanese/Vocabulario_kiriko.pdf"

# Store all the pages of the PDF in a variable

print("Escaneando el archivo ...")

pages = convert_from_path(PDF_file, dpi = 500)

print("Fichero escaneado, segmentando en paginas ...")

# Counter to store images of each page of PDF to image
image_counter = 1

print(type(pages))

# Iterate through all the pages stored above

for page in pages:
    # Declaring filename for each page of PDF as JPG
    # For each page, filename will be:
    # PDF page 1 -> page_1.jpg
    # PDF page 2 -> page_2.jpg
    # PDF page 3 -> page_3.jpg
    # ....
    # PDF page n -> page_n.jpg
    filename = "page_" + str(image_counter) + ".jpg"

    # Save the image of the page in system
    page.save(filename, 'JPEG')

    # Increment the counter to update filename
    image_counter = image_counter + 1

#------- Parte 2: - Recognizing text from the images using OCR

print("Reconociendo los caracteres ...")

# Variable to get count of total number of pages
filelimit = image_counter - 1

# Creating a text file to write the output
outfile = "out_text.txt"

# Open the file in append mode so that
# All contents of all images are added to the same file
f = open(outfile, "a")

custom_config =r'-l eng+jpn --psm 6' # Para que la salida sea en ENG, JPN

# Iterate from 1 to total number of pages
for i in range(1, filelimit+1):     #filelimit + 1
    # Set filename to recognize text from
    # Again, these files will be:
    # page_1.jpg
    # page_2.jpg

    # ....
    # page_n.jpg
    filename = "page_" + str(i) + ".jpg"

    print(f"Pagina {i} ...")

    # Recognize the text as string in image using pytesserct
    img = Image.open(filename)
    text = str(pytesseract.image_to_string(img,config=custom_config))
   # text = pytesseract.image_to_string(img)
   # text = str(((pytesseract.image_to_string(Image.open(filename, config=custom_config)))))

    # The recognized text is stored in variable text
    # Any string processing may be applied on text
    # Here, basic formatting has been done:
    # In many PDFs, at line ending, if a word can't
    # be written fully, a 'hyphen' is added.
    # The rest of the word is written in the next line
    # Eg: This is a sample text this word here GeeksF-
    # orGeeks is half on first line, remaining on next.
    # To remove this, we replace every '-\n' to ''.
    text = text.replace('-\n', '')

    # Finally, write the processed text to the file.
    f.write(text)

# Close the file after writing all the text.
f.close()