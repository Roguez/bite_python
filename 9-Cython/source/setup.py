# Desde aqui compilamos el codigo de cython

from distutils.core import Extension, setup
from Cython.Build import cythonize
import sys # Esto hay que ponerlo para que me trabaje con la version 3 de python
           # Que si no me pasa por defecto la 2
           # Haciendolo asi, siempre utiliza la version de python que estoy utilizando.

# define an extension that will be cythonized and compiled
ext = Extension(name="cython_module", sources=["cython_module.pyx"])
setup(ext_modules=cythonize(ext,compiler_directives={'language_level' : sys.version_info[0]}))

# Nota: compilar en la terminal con: python setup.py build_ext --inplace