""" Python wrapper for the C shared library mylib"""
import sys, platform
import ctypes, ctypes.util

# Find the library and load it
mylib_path = ctypes.util.find_library("mylib")
if not mylib_path:
    print("Unable to find the specified library.")
    sys.exit()

try:
    mylib = ctypes.CDLL(mylib_path)
except OSError:
    print("Unable to load the system C library")
    sys.exit()

'''

Please note that we don’t specify the library extension, find_library will match the system library extension.
This makes our code portable on most operating systems. A possible problem is that find_library uses the 
system library search path and the local folder. If you want to restrict the search path only to the local 
folder use something like:

   mylib_path = ctypes.util.find_library("./mylib")
 
Another potential problem is that, on UNIX like systems, find_library will search for both libmylib and mylib 
unless you specify the search path. When you search for your own libraries, it is probably a good ideas to prefer:

   mylib_path = ctypes.util.find_library("./mylib")
 
instead of the more general:

   mylib_path = ctypes.util.find_library("mylib")
   
'''