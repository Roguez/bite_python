# https://solarianprogrammer.com/2019/07/18/python-using-c-cpp-libraries-ctypes/

# Esto no vale desde Jupiter porque las funciones de C devuelven mas cosas,
# devuelven otras lineas con numeros tambi'en que no s'e de donde salen y
# el j'upiter solo te muestra la 'ultima linea, vamos que es un desastre.
# para trabajar con C debes hacerlo desde python real, nada de jupiter
# ni cosas parecidas.

"""Simple example of loading and using the system C library from Python"""

import sys, platform
import ctypes, ctypes.util

# Get the path to the system C library
if platform.system() == "Windows":
     path_libc = ctypes.util.find_library("msvcrt")
else:
     path_libc = ctypes.util.find_library("c")

# Get a handle to the sytem C library
try:
    libc = ctypes.CDLL(path_libc)
except OSError:
    print("Unable to load the system C library")
    sys.exit()

print(f'Succesfully loaded the system C library from "{path_libc}"')

# Entonces ahora podemos llamar cualquier librer'ia de C desde aqu'i
# solo debemos tener en cuenta que debemos pasarle los datos en el
# formato en que C los interpreta, por ejemplo los strings los espera
# como byte strings, por eso tenemos que pasarselos as'i

libc.puts(b"Hello from Python to C")

libc.printf(b"%s\n", b"Using the C printf function from Python ... ")

# Ojo que si le pasamos a C un objeto inmutable de python, este no podemos
# cambiarlo en C. (aqu'i tienes que estudiarte la librer'ia ctypes a fondo)
# Entonces si por ejemplo quisi'eramos pasar un string y modificarlo en C,
# primero deber'iamos aplicarle la funci'on de cytpes:
# create_string_buffer

# Create a mutable string
mut_str = ctypes.create_string_buffer(10)

# Fill the first 5 elements with 'X':
libc.memset(mut_str, ctypes.c_char(b"X"), 5)

# Print the modified string
libc.puts(mut_str)

# If you are careful, you can even do pointer arithmetic.
# Please note that this is not a good idea!
# Say that we want to add 4 O after the above 5 X in the mut_str.
# We’ll start by getting the address of the mut_str, add 5 to this and convert it to a char pointer:

# Add 4 'O' to the string starting from position 6
p = ctypes.cast(ctypes.addressof(mut_str) + 5, ctypes.POINTER(ctypes.c_char))

# Now, we can use memset to fill 4 slots with O:

libc.memset(p, ctypes.c_char(b"O"), 4)
# Finally, use puts to print the string:

libc.puts(mut_str)