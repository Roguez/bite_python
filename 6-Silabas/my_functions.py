import openpyxl   # https://openpyxl.readthedocs.io/en/stable/api/openpyxl.reader.excel.html
import re
import logging
import pandas as pd
import my_unicode

def get_xl_sheet(xl_file, xl_sheet):
    # Open xl_file and return xl_sheet Object.

    wb = openpyxl.load_workbook(xl_file, read_only = True)

    wb_sheet_Obj = wb.get_sheet_by_name(xl_sheet)

#    logging.info(f"Closing {xl_file} ...")
#    wb.close()   # No cierres todavia porque te da problemas, creo que
                  # lo que te devuelve no es mas que un puntero o asi

    return wb_sheet_Obj

def find_fonetic_column(xl_sheet_Obj):
    # MAIN sufix porque es una funcion experimental porque implemento con:
    # 1.- Objeto_xl directamente
    # 2.- Pandas
    # 3.- Numpy
    # 4.- Dask
    # 5.- Cython
    # 6.-
    # 7.-
    # Desde aqui solo hago las llamadas, registro el tiempo
    # que tarda cada una, muestro el resultado y devuelvo
    # por supuesto el dato requerido a main()
    # Y para que la comparativa sea mas fiable modifico
    # un poco el algoritmo para que en vez de pasarme la
    # primera columna donde encuentra una ə, pues que me
    # pase todas las columanas donde las encuentre.
    # Luego hago un set, y debe tener un solo valor, en caso
    # de tener mas debo reportar de un error y que se revise
    # el archivo porque hay varias columnas con fonetica
    # que me indique entonces cual elegir, entonces el usuario
    # tiene que ir al documento abrirlo, ver, y decidir que hace
    # con la opcion de pasarme por un input que le pongo el
    # numero correcto de la columna en ese caso.

    column_index = find_fonetic_column_xl_Obj(xl_sheet_Obj)
    return column_index
    find_fonetic_column_pandas(xl_sheet_Obj)

def find_fonetic_column_xl_Obj(xl_sheet_Obj):

    pattern = "[ə]"   # ə

    columns_with_matchs = []
    for row_WbObj in xl_sheet_Obj:
        for cellObj in row_WbObj:
            test_string = str(cellObj.value)
            result = re.search(pattern, test_string)
            if result: columns_with_matchs.append(cellObj.column)
    if len(columns_with_matchs) > 1:
        print("Existe mas de una columa con caracteres foneticos.")
        print("Por favor compruebe y seleccione la columna correcta")
        column_index = int(input(f" {set(columns_with_matchs)} : "))
    else:
        column_index = columns_with_matchs[0]
    return column_index

           # print(cellObj.coordinate, cellObj.value)

            # Construyo un set (ya que no permite duplicados), con cada uno de los
            # simbolos foneticos que ha recogido

            # caracteres = caracteres + str(cellObj.value)

def find_fonetic_column_pandas(xl_sheet_Obj):
   # https://www.soudegesu.com/en/post/python/pandas-with-openpyxl/    <<<<< ---------------*****

    logging.info(" Buscando con Pandas.")
    logging.info(" Convirtiendo a Pandas Data Frame")

    df = pd.DataFrame(xl_sheet_Obj.values)

    print(df.head(3))  # para ver que todo marcha

def get_number_of_syllabes(word):
    sillabes = 0
    if word == "": return sillabes

    vowels = my_unicode.IPA_vowels
    ignore = "ː(ˌ)"

    word_size = len(word)

    # El pricipio es especial porque no tengo posicion -1
    # como en el resto de posiciones.

    if word[0] in "ˌˈ(": init = 2
    else: init = 1

    # El final tambien es especial, porque no efectuo cambio
    # sobre vocal pero si en cambio es la ultima letra entonces
    # hay que incrementar.
    # əˈbɪlɪti

    if word[word_size - 1] in vowels: sillabes += 1

    # si son dos palabras tambien hay algo especial
    # ya que me salto el control inicial de la segunda
    # palabra, entonces tengo que hacer este truquito.
    # əˌmiːnəʊ ˈasɪd

    if " " in word:
        space_index = word.find(" ")
        # Tenemos que comprobar que no sean espacios en blanco al final de la palabra:
        total_size = len(word)
        if total_size - space_index >=2:
            print(word)
            if (word[space_index +1] in vowels) or ((word[space_index +1] == "'") and (word[space_index +2] in vowels)):
                sillabes +=1

    # Ahora el cuerpo central:
    # Basicamente le digo que no cambie en las vocales o en los simbolos para ignorar
    # y en lo que queda pues si es un acento y la anterior una consonate pues que cambie:
    # entonces aqui no cambia: ˌabs(ə)ntˈmʌɪndɪd
    # y en lo que queda si la anterior es una vocal o vocal larga(ː) pues que cambie
    # y listo.

    for index in range(init, word_size):

        if (word[index] in "ˈˌ") and (word[index-1] in vowels):
            sillabes +=1
            check1 = True
            check2 = True

        else:

            check1 = (word[index] not in vowels) and (word[index] not in ignore)
            check2 = word[index-1] in (vowels+'ː')
            check = check1 and check2
            if check: sillabes +=1

#        print(index,word[index-1],word[index], sillabes)
#        print(check1, check2)

    return sillabes

if __name__ == "__main__":

    test = ["əˈbʌɪd",
            "əbriːvɪˈeɪʃ(ə)n",
            "əˈbɪlɪti",
            "ˌabs(ə)ntˈmʌɪndɪd",
            "əksɛləˈreɪʃ(ə)n",
            "ˈalʌɪ",
            "əˌmiːnəʊ ˈasɪd",
            "ˈamfɪθiːətə",
            "ˌɑːkiˈɒlədʒi"]

    for i in list(range(0,len(test))):
        sillabes = get_number_of_syllabes(test[i])
        print(f"{test[i]}:  {sillabes} sillabes")