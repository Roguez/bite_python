
import logging
import my_unicode
import my_openpyxl

import my_functions
# funcion principal que pide los datos al usuario y llama al resto de funciones.

def main():

    # ----- Set parameters ----------------------
    xl_file = r"/home/roguez/Coding/Data/Listados/Excel/Vocabulario_de_inglés_A-B.xlsx"  # r = raw string
    xl_sheet = "Listado"
    # -------------------------------------------

    xl_sheet_Obj = my_functions.get_xl_sheet(xl_file, xl_sheet)
    xl_column_label = my_functions.find_fonetic_column_xl_Obj(xl_sheet_Obj)  #MAIN: pq implemento con varias
    print(f"El 'indice de la columna fonetica es: {xl_column_label}")
    columna = my_openpyxl.get_column_content(xl_file, xl_sheet, xl_column_label)


    silabas = {}
    for index in range(0, len(columna)):
        silabas[index] = [columna[index],my_functions.get_number_of_syllabes(columna[index])]

    print("##################################################################")
    for k, v in silabas.items():
        print(k, v)

"""
    listado_de_palabras = [""]
    for rowObj in xl_sheet_Obj.columns(xl_column_label):
        for cellObj in rowObj:
            listado_de_palabras.append(str(cellObj.value))
    print(f"El listado de palabras es: {listado_de_palabras}")
"""

#        worksheet.cell(row=r, column=c).value = value

if __name__ == "__main__":
    main()
#   unittest.main()