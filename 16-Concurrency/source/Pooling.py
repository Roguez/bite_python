


def pmap(lista, funcion):
    import multiprocessing
    procesos = multiprocessing.cpu_count()-1

    with multiprocessing.Pool(procesos) as p:
        return p.map(funcion, lista)

"""    len_lista = len(lista)
    entero, fraccional = divmod(len_lista, procesos)
    tope_enteros = int((entero -1)*procesos)
    if len_lista < procesos:
        print("len_lista < procesos")
        procesos = len_lista
        # en master_list las columnas representan los procesos y las filas
        # las sucesivas ejecuciones

        master_list = [lista[x:x+1] for x in range(0,len_lista)]
        return master_list
    elif fraccional == 0:
        print("fraccional == 0")
        master_list = [lista[x:x+entero] for x in range(0,len_lista,entero)]
        return master_list
    else:
        print("fraccional != 0")
        print(f"entero: {entero}")
        # Un numero de procesos "fraccional" van a tener un objeto mas que procesar:
        entero_plus = entero + 1
        len_lista_plus = entero_plus * fraccional
        submaster_list1 = [lista[x:x+entero_plus] for x in range(0,len_lista_plus,entero_plus)]
        mapping_list = [int(len_lista_plus/fraccional)]*fraccional
        print(mapping_list)

        # Un numero de procesos "(procesos-1)-fraccional" ejecutan 'entero' procesos:
        submaster_list2 = [lista[x:x+entero] for x in range(len_lista_plus, len_lista, entero)]
        master_list = submaster_list1 + submaster_list2

        return master_list


def multiproceso(funcion, nested_list):

    from multiprocessing import Pool

    procesos = len(nested_list)
    mapeo_procesos = [len(item) for item in nested_list]
    print(nested_list)
    print(f"procesos: {procesos}")
    print(f"Mapeo_procesos {mapeo_procesos}")

    with Pool(procesos) as p:
        result = (p.map(funcion, nested_list))
    return result
"""
if __name__ == "__main__":
    from timeit import timeit

    lista = [i for i in range(27)]
    print(lista)

    def func(x):
        return x*10

    result = pmap(lista,func)
    print(result)

