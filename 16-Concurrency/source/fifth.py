lista_original = [1,2,3,4,5,6,7,8,9,10]
procesos = 3
len_lista = len(lista_original)
sublista_size = int(len_lista/procesos)
print(sublista_size)

lista_copiada = [lista_original[x:x+sublista_size] for x in range(0, len(lista_original), sublista_size)]

print(lista_copiada)