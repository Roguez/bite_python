# Python code to demonstrate
# application of sleep()

import time

# initializing string
strn = "GeeksforGeeks"
print("hi")

# printing geeksforgeeks after delay
# of each character
for i in range(0, len(strn)):
    print(strn[i], end="")
    time.sleep(1)