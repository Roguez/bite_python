import multiprocessing

# we can find out the number of cpu cores available
print(multiprocessing.cpu_count())

# we can capture the process identifiers of any of our Python
# sub processes such as this:
print(multiprocessing.current_process().pid)



