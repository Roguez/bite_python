Representation of Graphs
------------------------

- Adjacency List Representations.

- Adjacency Matrix Representation.

    - For a simple graph (that has no loops), the adjacency matrix has 0s on the diagonal.
    - The adjacency matrix of an undirected graph is symmetric.
    - The memory use of an adjacency matrix is O(n2), where n is the number of nodes in the graph.
    - Number of 1s (or non-zero entries) in an adjacency matrix is equal to the number of edges
      in the graph.
    - The adjacency matrix for a weighted graph contains the weights of the edges connecting the
      nodes.

- Adjacency Lists vs Adjacency Matrices:

    - Adding new nodes in G is easy and straightforward when G is represented using an adjacency
      list. Adding new nodes in an adjacency matrix is a difficult task, as the size of the matrix
      needs to be changed and existing nodes may have to be reordered.


