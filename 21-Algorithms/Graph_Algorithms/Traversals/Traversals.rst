FDS: first deep search
BDS: first breath search

About 3D shapes:
https://www.bbc.co.uk/bitesize/guides/zcnb8mn/revision/1

Formally, a traversal is a systematic procedure for exploring a graph by examining 
all of its vertices and edges. A traversal is efficient if it visits all the vertices
and edges in time proportional to their number, that is, in linear time.
Graph traversal algorithms are key to answering many fundamental questions
about graphs involving the notion of reachability, that is, in determining how to
travel from one vertex to another while following paths of a graph.

