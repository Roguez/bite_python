import time

def main():
    n = 35

    ts = time.time()
    result = fib_simple_rec(n)
    te = time.time()
    print(result, 'took: ', te - ts)

    ts = time.time()
    result = fib_imperative_stateful_variables(n)
    te = time.time()
    print(result, 'took: ', te - ts)

    ts = time.time()
    result = fact_tail(n)
    te = time.time()
    print(result, 'took: ', te - ts)

    ts = time.time()
    result = fact_imperativ_opt(n)
    te = time.time()
    print(result, 'took: ', te - ts)

def timeit(f):
    def timed(*args, **kw):
        ts = time.time()
        result = f(*args, **kw)
        te = time.time()

        print('func:%r args:[%r, %r] took: %2.4f sec' % (f.__name__, args, kw, te - ts))
        return result

    return timed

def fib_simple_rec(n):
    #   Calculate fibonacci
    if n == 0: return 0
    if n == 1: return 1
    return fib_simple_rec(n - 1) + fib_simple_rec(n - 2)

def fib_imperative_stateful_variables(n):
    #   Calculate fibonacci
    if n == 0: return 0
    if n == 1: return 1
    f_n2, f_n1 = 1, 1
    for i in range(3, n + 1):
        f_n2, f_n1 = f_n1, f_n2 + f_n1
    return f_n1

def fact_tail(n):
    #   Calculate factorial
    if n == 0:
        return 1
    else:
        return n * fact_tail(n - 1)

def fact_imperativ_opt(n):
    #   Calculate factorial
    if n == 0: return 1
    f = 1
    for i in range(2, n):
        f = f * i
    return f

if __name__ == "__main__":
    # execute only if run as a script
    main()
