#    Se trata de ejecutar un bucle sobre una lista, hasta el ultimo elemento
#    Entonces se trata de crear una funcion a la que le paso el nombre del
#    directorio y los par'ametros que en caso de no pasarselos se queda con los valores por 
#    defecto, y luego pongo todo el codigo ordenado y lo encapsulo en una funci'on.
#    que tiene dentro un bucle que lo ejecuta sobre todo archivo que encuentre en ese directorio.
#    Asi es la primera versi'on, a lo bruto, ya iremos optimizando.

#-------------------  A mejorar ---------------

# 1) Da error si encuentra algun fichero que no es .mp3
# 2) Cuando te pide al path no efectua ninguna comprobacion de ningun tipo
# 3) Que ejecute en paralelo con la CPU
# 4) Que ejecute en paralelo con la GPU, es posible?
#-----------------------------------------------

# Import STLs

from os import listdir, mkdir

# Import non STLs

import pydub   # Debes tener instalado ffmpeg

def procesa_sonido(sound, Export_Folder, counter_b, pistas_SIN_path):

    start_trim = detect_leading_silence(sound)
    end_trim = detect_leading_silence(sound.reverse())

    duration = len(sound)
    trimmed_sound = sound[start_trim:duration - end_trim]

    # Silencio inicial de 200 ms

    Silencio_inicial = pydub.AudioSegment.silent(duration=200)

    # Silencio final

    Duracion_sonido = len(trimmed_sound)

    if Duracion_sonido < 1000:
        Silencio_extra = pydub.AudioSegment.silent(duration=1000)
    elif Duracion_sonido >= 1000 and Duracion_sonido < 2000:
        Silencio_extra = pydub.AudioSegment.silent(duration=0)
    else:
        Silencio_extra = pydub.AudioSegment.silent(duration=0)

    Silencio_final = pydub.AudioSegment.silent(Duracion_sonido * 2) + Silencio_extra

    # Concatenos los silencios con el sonido

    # ------  hago un hackeo, borrar la linea inferior:

    # Silencio_final = pydub.AudioSegment.silent(duration=100)  # -------- esto es un hacking, borrarlo

    # ------- borrar la linea superior, es un hackeo.

    Audio_to_Export = Silencio_inicial + trimmed_sound + Silencio_final

    # Asignamos un nombre a los ficheros que vamos a exportar.

    Silencios_Editados_identifier = "_Silencios_editados"
    nombre, extension = pistas_SIN_path[counter_b].split('.')
    pistas_Silencios_editados = []
    pistas_Silencios_editados.append(nombre + Silencios_Editados_identifier + '.' + extension)

    File_to_Export = Export_Folder + "/" + pistas_SIN_path[counter_b]
    counter_b += 1

    # Exportamos

    Audio_to_Export.export(File_to_Export, format="mp3")

def detect_leading_silence(sound, silence_threshold=-50.0, chunk_size=5):
    '''
    sound is a pydub.AudioSegment
    silence_threshold in dB
    chunk_size in ms

    iterate over chunks until you find the first one with sound
    '''
    trim_ms = 0 # ms

    assert chunk_size > 0 # to avoid infinite loop (assert tell the program to test that condition,
                          # and immediately trigger an error if the condition is false.)
        
    while sound[trim_ms:trim_ms+chunk_size].dBFS < silence_threshold and trim_ms < len(sound):
        trim_ms += chunk_size

    return trim_ms


def word_silences():

    # Indicamos el directorio que contiene las pistas a ser modificadas

    # path = input("Please, introduce the folder path: ")

    path = "/home/fer/coding/data/audio/ingles"

     # Cargamos todos los nombres de ficheros del directorio en una lista

    pistas_SIN_path = listdir(path)
        
     # Me hago otra lista donde tengo los nombres largos:

    pistas_CON_path = pistas_SIN_path.copy() # Entonces ahora tienes una lista con tantos punteros como la lista 
                                # que vas a usar para pasarle datos. Y como vas a sobreescribirlos 
                                # todos pues no hay problema.
                                # fijate que si hiceras:
                                # pistas_con_path = pistas
                                # Cuando, asignas una lista a otra, lo que haces es pasar una referencia a 
                                # la primera lista. en cambio, con un copy creas una nueva instancia independiente.
    counter_a = 0                        
    for pista in pistas_SIN_path:
        pistas_CON_path[counter_a] = path + "/" + pistas_SIN_path[counter_a]
        counter_a += 1 
     
    # Creamos la carpeta donde vamos a guardar las pistas editadas
    
    Export_Folder = "/Con_silencios_editados"
    Export_Folder = path + Export_Folder

    try:
        mkdir(Export_Folder)
    except OSError:
        print ("Creation of the directory %s failed" % Export_Folder)
    else:
        print ("Successfully created the directory %s" % Export_Folder)
    
    ###### Comenzamos el bucle chupy guay de ir pista por pista:
    
    counter_b = 0
    for pista in pistas_SIN_path:

        print(f"Procesando: \"{pistas_SIN_path[counter_b]}\"")
        try:
            sound = pydub.AudioSegment.from_file(pistas_CON_path[counter_b],format="mp3")
        except Exception as e:
            print(f"\n\tSe ha producido una excepción: {pistas_SIN_path[counter_b], e.__class__}")
        else:
            procesa_sonido(sound, Export_Folder, counter_b, pistas_SIN_path)
        finally:
            counter_b += 1

if __name__ == "__main__":
    word_silences()

