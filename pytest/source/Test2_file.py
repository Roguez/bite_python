import pytest

def greeting(name, greet_word="Hello"):
    return f"{greet_word}, {name}"

#@pytest.mark.parametrize()

@pytest.mark.parametrize(
   "name, expected",
   [

       pytest.param("Kate", "Hello, Kate", id = "name_Kate"),
       pytest.param("Karlis", "Hello, Karlis", id = "name_Karlis"),
       # ("Tim", "Hi, Tim"),
   ]
)

def test_greeting(name, expected):
    assert greeting(name) == expected