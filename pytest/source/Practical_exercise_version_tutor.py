from datetime import datetime
# get current date
now = datetime.now()
curtimestamp = now.strftime("%Y%m%d-%H:%M:%S")
# read the text file with our message log
print('Reading messages.txt')
try:
    with open('messages.txt') as file:
        for line in file:
            print(line)
except FileNotFoundError:
    passwhile True:
    # ask for new input
    toAdd = input('Please enter a new message (type quit to exit): ')
    if toAdd == "quit":
        print("Bye!")
        break
    # string to write
    if toAdd:
        with open('messages.txt', 'a') as f:
            f.write(f'{curtimestamp}, {toAdd}\n')