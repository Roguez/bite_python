def test_first():
    assert True

def test_second():
    assert False

def test_third():
    1/0  # da error de division por cero aunque no tengamos "assert"

def test_identity_fail():
    assert (False is True) is False

def test_exception_fail():
    import pytest
    with pytest.raises(ZeroDivisionError):
         1/0
    # except ZeroDivisionError
    try:    # we dont need it.
        1/0 # we dont need it.
    except ZeroDivisionError: # we dont need it.
        assert True, "" # we dont need it.



