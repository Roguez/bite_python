from datetime import datetime

import pytest

from prog import read_from_file, read_line


def test_file_operations(file_data):
    assert read_line(file_data) == (datetime(2020, 1, 18, 13, 49, 47), "text")


def test_read_from_file(existing_file):
    buffer, parsed = read_from_file(existing_file)

    assert parsed == []