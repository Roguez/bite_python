import pytest

def greeting(name, greet_word="Hello"):
    return f"{greet_word}, {name}"

@pytest.mark.parametrize(
    "greet_word",
    [
        pytest.param("Moi"),
        pytest.param("Tere"),
    ]
)
@pytest.mark.parametrize(
   "name, expected",
   [

       pytest.param("Kate", id = "name_Kate"),
       pytest.param("Karlis", id = "name_Karlis"),

   ]
)

def test_greeting(name, greet_word):
    assert greeting(name, greet_word) == f"{greet_word}, {name}"