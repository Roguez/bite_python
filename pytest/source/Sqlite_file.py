# documentation: https://docs.python.org/3.7/library/sqlite3.html
# https://www.sqlitetutorial.net/sqlite-python/sqlite-python-select/

def create_connection(db_file):
    # param db_file: database file
    # return: Connection object or None
    import sqlite3

    conn = None
    try:
        conn = sqlite3.connect(db_file)   # if it doesn't exist then it will create a new one.
    except Error as e:
        print(e)
    return conn

def create_table_if_not_exist(conn, table1):
    # Creating a table with 2 columns
    # and setting the first collumn (time) as PRIMARY KEY

    cur = conn.cursor()
    sql = f"CREATE TABLE IF NOT EXISTS {table1} (time_date text PRIMARY KEY, message text)"
    cur.execute(sql)

def write_in_db(conn, table1, message_content):

    if message_content == "quit":
        return False

    data = [(get_current_date_time(), message_content)]

    sql = f"INSERT INTO {table1}(time_date,message) VALUES (?,?)"

    conn.executemany(sql, data)
    conn.commit()
    return True

def read_table(conn,table1):
    """
    Query all rows in the tasks table
    :param conn: the Connection object
    :return:
    """

    cur = conn.cursor()
    cur.execute(f"SELECT * FROM {table1}")

    rows = cur.fetchall()    # fetchall, return a list with rows of a query result.

    return rows

def print_table(rows):

    for row in rows:
        print(row)

# create a cursor objet to call its execute() method to perform SQL commands.
def get_new_message():

    toAdd = input('Please enter a new message (type quit to exit): ')
    return toAdd

def get_current_date_time():
    from datetime import datetime

    # get current date
    now = datetime.now()
    curtimestamp = now.strftime("%d%m%Y-%H:%M:%S")
    return curtimestamp

def close_db(conn):

    try:
        conn.close()
    except:
        print("--Error closing the database")
    else:
        print("--Disconected suscesfully from data base.")
    finally:
        print("Bye!")

def main():

    db_file = r"messages.db"
    working_table = "messaging_records"

    # create a database connection
    conn = create_connection(db_file)
    create_table_if_not_exist(conn, working_table)

    print(f"\n-- Reading registers in {db_file} (sqlite3): \n")
    print_table(read_table(conn, working_table))

    while (write_in_db(conn, working_table, get_new_message())):
          print(f"--Recorder in {db_file}")

    close_db(conn)

if __name__ == '__main__':
    main()

