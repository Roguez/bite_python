from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def home_page_view(request):
    return HttpResponse('This is my blog home page!')

def development_view(request):
    return HttpResponse('This the development section of my blog')
