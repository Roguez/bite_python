def greet(name):

    message = "Hello, " + name

    return message


if __name__ == "__main__":

    name = "Datacamp"

    print(f"{greet(name)}")
