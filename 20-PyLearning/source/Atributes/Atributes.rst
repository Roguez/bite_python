Attribute interception techniques
---------------------------------

-   In most cases, the attribute lives in the object itself, or is inherited from a class from
    which it derives.

-   We need to master the domain of allowing our scripts to dynamically compute attribute values when
    fetching them and validate or change attribute values when storing them. With this propose we will
    introduce 4 techniques:

        1. The *__getattr__* and *__setattr__* **methods** (we will overload this methods) , for routing undefined attribute fetches
           and all attribute assignments to generic handler methods.

        2. The *__getattribute__* **method** (we will overload this method), for routing all attribute fetches to a generic handler
           method.

        3. The *property* **built-in**, for routing specific attribute access to get and set handler
           functions.

        4. The *descriptor* **protocol**, for routing specific attribute accesses to instances of classes
           with arbitrary get and set handler methods, and the basis for other tools such as
           properties and slots.

All four techniques share goals to some degree, and it’s usually possible to
code a given problem using any one of them.

The last two techniques listed here apply to specific attributes, whereas the first two are
generic enough to be used by delegation-based proxy classes that must route arbitrary attributes
to wrapped objects.

These techniques outlined here are useful to code **decorators**

The **property protocol** allows us to route a specific attribute’s **get**, **set**, and **delete**
operations to functions or methods we provide.

The __getattr__ and __getattribute__ intercept arbitrary names. By contrast, we must define
one property or descriptor for every attribute we wish to intercept.

These two methods are representatives of a set of attribute interception methods
that also includes __setattr__ and __delattr_. To catch attribute
changes by assignment, we must code a __setattr__ method—an operator
overloading method run for every attribute fetch, **which must take care to avoid recursive
loops by routing attribute assignments through the instance namespace dictionary
or a superclass method** **WHAT??**. Although less common, we can also code a __delattr__ overloading
method (which must avoid looping in the same way) to intercept attribute
deletions.

By contrast, properties and descriptors catch get, set, and delete operations
by design.

Unlike properties and descriptors, these methods are part of Python’s general operator
overloading protocol.




















