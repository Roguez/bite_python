**¿Why using the module ABC to create and abstract class?**

I mean that we just could write a class like this:

.. code-block:: python

    class abstract_class:
        def abstract_method1(self)
            pass

        def abstract_method2(self)
            pass

And then use it like the blueprint or skeleton of others classes.
So all of them would have these methods in common as they are
inheriting from the supper (abstract now) class:

.. code-block:: python

    class factory1(abstract_class)
        pass

    class factory2(abstract_class)
        pass

But then this people say, no no in these cases we shall use:

.. code-block:: python

    from abc import ABC, ABCMeta, abstractmethod

And then something like:

..  code-block:: python

    from abc import ABCMeta, abstractmethod

    class abstract_class_abc(metaclass=ABCMeta):

        @abstractmethod
        def abstract_method1(self):
            pass

        @abstractmethod
        def abstract_method2(self):
            pass




**Why? Which are the extra features?**

- Well the abstract_class_abc, wont allow instantiation, it will
raise an exception, as the concept of abstract class involve inheritance
but not instantiation. It is a class that we make to build subclasses but
not for instantiation.

    - ** But which is the problem if we just instantiate it?** Well
    it has no sense, as it abstract methods are empty, they should
    be implemented within it subclasses. So it is a way to avoid an
    eventually bug.

- It will also raise and exception if we try to instantiate any of
  its subclases before implementing all its methods defined as
  abstract methods with the decorator @abstractmethod in the
  abstract_class. So you cant instantiate a subclass of an abstract
  class before implement in the subclass all the abstract methods
  inherited from the abstract class.

**Using ABC or (metaclass=ABCMeta) ??**

  Yo creo que el segundo es mas seguro pero no se explicarlo.











