- First read the attributes section as I introduce there 4 attribute
  interception techniques.

Decorators
----------

- A decorator is a function that takes a function as its only parameter
  and returns a function. This is helpful to “wrap” functionality with
  the same code over and over again.

- The syntax is like this:

..  code-block:: python

    def func1(func3)
        pass
        return func4

    @func1
    def func2()
        pass

And the call is somehow like this:

..  code-block:: python

    func2()

