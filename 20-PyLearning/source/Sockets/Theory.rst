https://www.youtube.com/watch?v=u4kr7EFxAKk

Seguir por el minuto 4!
-----------------------

- Internet está llena de nodos que se hablan entre ellos.
- Pueden ser client-server o incluso client-client (p2p network)
- Let focus in the scenario client-server.
- The servers are listening to the clients.
- So the client send a request and the server send a respond.
- Sockets are the base of internet.
- Important concepts:
    - Port number: we have a machine with and IP, and this machine
      provide multiples services. Every service will have a different
      port number. Some services are for instance: HTTP, MAIL SERVICE.

      When our code create a server we have to assign it a free port
      number.

      **How can I know which are the free port numbers?**

    - The type of connection we are going to build: TCP or UDP.
        - TCP: Transmission Control Protocol, first we have to create
          connection, and then we can communicate.
        - UDP: User Datagram Protocol, we don't create any connection,
          we just send the packed. The only drawback is that you don't
          have a way to know if the packed reached the destination.

Seguir por el minuto 4
----------------------
